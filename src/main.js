import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'

axios.defaults.baseURL = 'http://3.137.183.236';

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
